
# coding: utf-8

import numpy as np
import pandas
import random

def formatCut(cut):
    if(cut == "Fair"):
        return 0
    elif(cut == "Good"):
        return 1
    elif(cut == "Very Good"):
        return 2
    elif(cut == "Premium"):
        return 3
    elif(cut == "Ideal"):
        return 4

def formatColor(color):
    if(color == "J"):
        return 0
    elif(color == "I"):
        return 1
    elif(color == "H"):
        return 2
    elif(color == "G"):
        return 3
    elif(color == "F"):
        return 4
    elif(color == "E"):
        return 5
    elif(color == "D"):
        return 6

def formatClarity(clarity):
    if(clarity == "I3"):
        return 0
    elif(clarity == "I2"):
        return 1
    elif(clarity == "I1"):
        return 2
    elif(clarity == "SI2"):
        return 3
    elif(clarity == "SI1"):
        return 4
    elif(clarity == "VS2"):
        return 5
    elif(clarity == "VS1"):
        return 6
    elif(clarity == "VVS2"):
        return 7
    elif(clarity == "VVS1"):
        return 8
    elif(clarity == "IF"):
        return 9
    elif(clarity == "FL"):
        return 10

def restoreColumnValues(value):
	if(value["cut"] == 0):
		value["cut"] = "Fair"
	elif(value["cut"] == 1):
		value["cut"] = "Good"
	elif(value["cut"] == 2):
		value["cut"] = "Very Good"
	elif(value["cut"] == 3):
		value["cut"] = "Premium"
	elif(value["cut"] == 4):
		value["cut"] = "Ideal"

	if(value["color"] == 0):
		value["color"] = "J"
	elif(value["color"] == 1):
		value["color"] = "I"
	elif(value["color"] == 2):
		value["color"] ="H"
	elif(value["color"] == 3):
		value["color"] = "G"
	elif(value["color"] == 4):
		value["color"] = "F"
	elif(value["color"] == 5):
		value["color"] = "E"
	elif(value["color"] == 6):
		value["color"] = "D"

	if(value["clarity"] == 0):
		value["clarity"] = "I3"
	elif(value["clarity"] == 1):
		value["clarity"] = "I2"
	elif(value["clarity"] == 2):
		value["clarity"] = "I1"
	elif(value["clarity"] == 3):
 		value["clarity"] = "SI2"
	elif(value["clarity"] == 4):
		value["clarity"] = "SI1"
	elif(value["clarity"] == 5):
		value["clarity"] = "VS2"
	elif(value["clarity"] == 6):
		value["clarity"] = "VS1"
	elif(value["clarity"] == 7):
		value["clarity"] = "VVS2"
	elif(value["clarity"] == 8):
		value["clarity"] = "VVS1"
	elif(value["clarity"] == 9):
		value["clarity"] = "IF"
	elif(value["clarity"] == 10):
		value["clarity"] = "FL"

	return value

def updateColumnValues(value):
    if(value["cut"] == "Fair"):
        value["cut"] = 0
    elif(value["cut"] == "Good"):
        value["cut"] = 1
    elif(value["cut"] == "Very Good"):
        value["cut"] = 2
    elif(value["cut"] == "Premium"):
        value["cut"] = 3
    elif(value["cut"] == "Ideal"):
        value["cut"] = 4

    if(value["color"] == "J"):
        value["color"] = 0
    elif(value["color"] == "I"):
        value["color"] = 1
    elif(value["color"] == "H"):
        value["color"] = 2
    elif(value["color"] == "G"):
        value["color"] = 3
    elif(value["color"] == "F"):
        value["color"] = 4
    elif(value["color"] == "E"):
        value["color"] = 5
    elif(value["color"] == "D"):
        value["color"] = 6

    if(value["clarity"] == "I3"):
        value["clarity"] = 0
    elif(value["clarity"] == "I2"):
        value["clarity"] = 1
    elif(value["clarity"] == "I1"):
        value["clarity"] = 2
    elif(value["clarity"] == "SI2"):
        value["clarity"] = 3
    elif(value["clarity"] == "SI1"):
        value["clarity"] = 4
    elif(value["clarity"] == "VS2"):
        value["clarity"] = 5
    elif(value["clarity"] == "VS1"):
        value["clarity"] = 6
    elif(value["clarity"] == "VVS2"):
        value["clarity"] = 7
    elif(value["clarity"] == "VVS1"):
        value["clarity"] = 8
    elif(value["clarity"] == "IF"):
        value["clarity"] = 9
    elif(value["clarity"] == "FL"):
        value["clarity"] = 10

    return value

def getUserInput():
    price = float(raw_input("Entre o preço (326-18823): "))
    carat = float(raw_input("Entre o valor do peso (0-5.03): "))
    cut = str(raw_input("Entre o valor da lapidaçao (Fair, Good, Very Good, Premium, Ideal): "))
    color = str(raw_input("Entre o valor da cor (D, E, F, G , H, I, J): "))
    clarity = str(raw_input("Entre o valor da pureza (I3, I2, I1, SI2, SI1, VS2, VS1, VVS2, VVS1, IF, FL): "))
    depth = float(raw_input("Entre o valor da espessura (43-79): "))
    table = float(raw_input("Entre o valor da mesa (43-95): "))

    inputDiamond = np.array([carat, depth, table, price, formatCut(cut), formatColor(color), formatClarity(clarity)])

    inputDiamond = inputDiamond / np.linalg.norm(inputDiamond)

    return inputDiamond

def getDiamondByIndex(index, csv):
    diamond = np.array([float(csv.iloc[index]["carat"]),
                        float(csv.iloc[index]["depth"]),
                        float(csv.iloc[index]["table"]),
                        float(csv.iloc[index]["price"]),
                        formatCut(csv.iloc[index]["cut"]),
                        formatColor(csv.iloc[index]["color"]),
                        formatClarity(csv.iloc[index]["clarity"])])

    diamond = diamond / np.linalg.norm(diamond)

    return diamond

def putOnTop(top, index, metric):
    topLen = len(top)

    if(topLen < 5):
        top[index] = metric
    else:
        maxTopValue = max(top.values())

        if(random.uniform(0, 1) > 0.25 and metric < maxTopValue):
            for key in top:
                if(top[key] == maxTopValue):
                    del top[key]
                    top[index] = metric
                    break;
        #print(maxTopValue)
        #print(metric)
        #print(top)
        #sd = raw_input()

    return top

def presentTop(top, csv):
    print("\n")

    for key in top:
        print("Diamante " + str(key))
        print("Preço: " + str(csv.iloc[int(key)]["price"]))
        print("Peso: " + str(csv.iloc[int(key)]["carat"]))
        print("Lapidacao: " + str(csv.iloc[int(key)]["cut"]))
        print("Cor: " + str(csv.iloc[int(key)]["color"]))
        print("Pureza: " + str(csv.iloc[int(key)]["clarity"]))
        print("Espessura: " + str(csv.iloc[int(key)]["depth"]))
        print("Mesa: " + str(csv.iloc[int(key)]["table"]) + "\n")

def getMostSimilarDiamonds(inputDiamond, diamondsMatrix, n):
    top = {}
    for i in range (n):
        #print(top)
        top = putOnTop(top, i, np.dot(inputDiamond, diamondsMatrix[i]))

    return top

def chooseNewDiamond():
    print("Voce esta satisfeito com os diamantes apresentados?")

    inputId = int(raw_input("Se nao, entre o indice do que voce \n mais gostou para encontrarmos outros semelhantes: "))

    return inputId

n = 50000

diamondsCsv = pandas.read_csv("diamonds.csv").head(n)
del diamondsCsv["x"]
del diamondsCsv["y"]
del diamondsCsv["z"]
del diamondsCsv['Unnamed: 0']

diamondsCsv = diamondsCsv.apply(updateColumnValues, axis=1)

diamondsMatrix = diamondsCsv.values

diamondsMatrix = diamondsMatrix/diamondsMatrix.sum(axis=-1,keepdims=True)

inputDiamond = getUserInput()

diamondsCsv = diamondsCsv.apply(restoreColumnValues, axis=1)

while(True):
    top = getMostSimilarDiamonds(inputDiamond, diamondsMatrix, n)

    presentTop(top, diamondsCsv)

    inputDiamond = getDiamondByIndex(chooseNewDiamond(), diamondsCsv)