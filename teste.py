
# coding: utf-8

import tkinter
from PIL import Image, ImageTk
import pandas

def restoreColumnValues(value):
    if(value["cut"] == 0):
        value["cut"] = "Fair"
    elif(value["cut"] == 1):
        value["cut"] = "Good"
    elif(value["cut"] == 2):
        value["cut"] = "Very Good"
    elif(value["cut"] == 3):
        value["cut"] = "Premium"
    elif(value["cut"] == 4):
        value["cut"] = "Ideal"

    if(value["color"] == 0):
        value["color"] = "J"
    elif(value["color"] == 1):
        value["color"] = "I"
    elif(value["color"] == 2):
        value["color"] ="H"
    elif(value["color"] == 3):
        value["color"] = "G"
    elif(value["color"] == 4):
        value["color"] = "F"
    elif(value["color"] == 5):
        value["color"] = "E"
    elif(value["color"] == 6):
        value["color"] = "D"

    if(value["clarity"] == 0):
        value["clarity"] = "I3"
    elif(value["clarity"] == 1):
        value["clarity"] = "I2"
    elif(value["clarity"] == 2):
        value["clarity"] = "I1"
    elif(value["clarity"] == 3):
        value["clarity"] = "SI2"
    elif(value["clarity"] == 4):
        value["clarity"] = "SI1"
    elif(value["clarity"] == 5):
        value["clarity"] = "VS2"
    elif(value["clarity"] == 6):
        value["clarity"] = "VS1"
    elif(value["clarity"] == 7):
        value["clarity"] = "VVS2"
    elif(value["clarity"] == 8):
        value["clarity"] = "VVS1"
    elif(value["clarity"] == 9):
        value["clarity"] = "IF"
    elif(value["clarity"] == 10):
        value["clarity"] = "FL"

    return value

def formatCut(cut):
    if(cut == "Fair"):
        return 0
    elif(cut == "Good"):
        return 1
    elif(cut == "Very Good"):
        return 2
    elif(cut == "Premium"):
        return 3
    elif(cut == "Ideal"):
        return 4

def formatColor(color):
    if(color == "J"):
        return 0
    elif(color == "I"):
        return 1
    elif(color == "H"):
        return 2
    elif(color == "G"):
        return 3
    elif(color == "F"):
        return 4
    elif(color == "E"):
        return 5
    elif(color == "D"):
        return 6

def formatClarity(clarity):
    if(clarity == "I3"):
        return 0
    elif(clarity == "I2"):
        return 1
    elif(clarity == "I1"):
        return 2
    elif(clarity == "SI2"):
        return 3
    elif(clarity == "SI1"):
        return 4
    elif(clarity == "VS2"):
        return 5
    elif(clarity == "VS1"):
        return 6
    elif(clarity == "VVS2"):
        return 7
    elif(clarity == "VVS1"):
        return 8
    elif(clarity == "IF"):
        return 9
    elif(clarity == "FL"):
        return 10

def clearListFrame(frame):
    for child in frame.winfo_children():
        child.destroy()

#callback para o botão Buscar
def buttonCallback():
    minCaratValue = float(minCaratEntry.get())
    maxCaratValue = float(maxCaratEntry.get())

    minDepthValue = float(minDepthEntry.get())
    maxDepthValue = float(maxDepthEntry.get())

    minTableValue = float(minTableEntry.get())
    maxTableValue = float(maxTableEntry.get())

    minPriceValue = float(minPriceEntry.get())
    maxPriceValue = float(maxPriceEntry.get())

    minCutValue = int(formatCut(minCutList.get(tkinter.ACTIVE)))
    maxCutValue = float(formatCut(maxCutList.get(tkinter.ACTIVE)))

    minColorValue = float(formatColor(minColorList.get(tkinter.ACTIVE)))
    maxColorValue = float(formatColor(maxColorList.get(tkinter.ACTIVE)))

    minClarityValue = float(formatClarity(minClarityList.get(tkinter.ACTIVE)))
    maxClarityValue = float(formatClarity(maxClarityList.get(tkinter.ACTIVE)))

    csv = getCsv()

    csv_alter = csv.loc[(csv["carat"] >= minCaratValue) &
                        (csv["carat"] <= maxCaratValue) &
                        (csv["depth"] >= minDepthValue) &
                        (csv["depth"] <= maxDepthValue) &
                        (csv["table"] >= minTableValue) &
                        (csv["table"] <= maxTableValue) &
                        (csv["price"] >= minPriceValue) &
                        (csv["price"] <= maxPriceValue) &
                        (csv["cut"] >= minCutValue) &
                        (csv["cut"] <= maxCutValue) &
                        (csv["color"] >= minColorValue) &
                        (csv["color"] <= maxColorValue) &
                        (csv["clarity"] >= minClarityValue) &
                        (csv["clarity"] <= maxClarityValue)]

    csv_alter = csv_alter.apply(restoreColumnValues, axis=1)

    listingFrame = getListFrame()
    clearListFrame(listingFrame)

    for row in csv_alter.itertuples():
        rowFrame = tkinter.Frame(listingFrame, borderwidth = 1, relief="solid")
        diamondLabel = tkinter.Label(rowFrame, text="Lapidaçao: " + str(getattr(row, "cut")) + "\n" +
                                                    "Espessura: " + str(getattr(row, "depth")) + "\n" +
                                                    "Preço: " + str(getattr(row, "price")) + "\n" +
                                                    "Mesa: " + str(getattr(row, "table")) + "\n" +
                                                    "Peso: " + str(getattr(row, "carat")) + "\n" +
                                                    "Cor: " + str(getattr(row, "color")) + "\n" +
                                                    "Claridade: " + str(getattr(row, "clarity")) + "\n")
        diamondLabel.grid()
        rowFrame.grid()

def updateColumnValues(value):
    if(value["cut"] == "Fair"):
        value["cut"] = 0
    elif(value["cut"] == "Good"):
        value["cut"] = 1
    elif(value["cut"] == "Very Good"):
        value["cut"] = 2
    elif(value["cut"] == "Premium"):
        value["cut"] = 3
    elif(value["cut"] == "Ideal"):
        value["cut"] = 4

    if(value["color"] == "J"):
        value["color"] = 0
    elif(value["color"] == "I"):
        value["color"] = 1
    elif(value["color"] == "H"):
        value["color"] = 2
    elif(value["color"] == "G"):
        value["color"] = 3
    elif(value["color"] == "F"):
        value["color"] = 4
    elif(value["color"] == "E"):
        value["color"] = 5
    elif(value["color"] == "D"):
        value["color"] = 6

    if(value["clarity"] == "I3"):
        value["clarity"] = 0
    elif(value["clarity"] == "I2"):
        value["clarity"] = 1
    elif(value["clarity"] == "I1"):
        value["clarity"] = 2
    elif(value["clarity"] == "SI2"):
        value["clarity"] = 3
    elif(value["clarity"] == "SI1"):
        value["clarity"] = 4
    elif(value["clarity"] == "VS2"):
        value["clarity"] = 5
    elif(value["clarity"] == "VS1"):
        value["clarity"] = 6
    elif(value["clarity"] == "VVS2"):
        value["clarity"] = 7
    elif(value["clarity"] == "VVS1"):
        value["clarity"] = 8
    elif(value["clarity"] == "IF"):
        value["clarity"] = 9
    elif(value["clarity"] == "FL"):
        value["clarity"] = 10

    return value

#criando janela
main = tkinter.Tk()
main.title("Diamond Recommender System")
main.geometry("900x900")
main.grid_rowconfigure(0, weight=0)
main.grid_columnconfigure(0, weight=1)

#criando o frame de botões e de listagem
constraintFrame = tkinter.Frame(main, borderwidth=1, relief = "solid")
constraintFrame.grid()

listFrame = tkinter.Frame(main)
listFrame.grid()

def getListFrame():
    return listFrame

#criando o frame do peso
caratFrame = tkinter.Frame(constraintFrame, borderwidth=1, relief="solid")
caratFrame.grid(row=0, column = 0, padx=10,pady=10)

caratLabel = tkinter.Label(caratFrame, text="Peso(ct)", )
caratLabel.grid(sticky = 'E')

minMaxCaratFrame = tkinter.Frame(caratFrame)
minMaxCaratFrame.grid()

minCarat = tkinter.StringVar()
minCaratLabel = tkinter.Label(minMaxCaratFrame, text="min")
minCaratLabel.grid(sticky = 'W')
minCaratEntry = tkinter.Entry(minMaxCaratFrame, textvariable=minCarat)
minCaratEntry.insert(tkinter.END, 0.2)
minCaratEntry.grid()

maxCarat = tkinter.StringVar()
maxCaratLabel = tkinter.Label(minMaxCaratFrame, text="max")
maxCaratLabel.grid(sticky = 'W')
maxCaratEntry = tkinter.Entry(minMaxCaratFrame, textvariable=maxCarat)
maxCaratEntry.insert(tkinter.END, 5.01)
maxCaratEntry.grid()

#criando o frame da espessura
depthFrame = tkinter.Frame(constraintFrame, borderwidth=1, relief="solid")
depthFrame.grid(row = 0, column = 1,padx=10,pady=10)
depthLabel = tkinter.Label(depthFrame, text="Espessura(%)", )
depthLabel.grid(sticky = 'E')

minMaxDepthFrame = tkinter.Frame(depthFrame)
minMaxDepthFrame.grid()

minDepth = tkinter.StringVar()
minDepthLabel = tkinter.Label(minMaxDepthFrame, text="min")
minDepthLabel.grid(sticky = 'W')
minDepthEntry = tkinter.Entry(minMaxDepthFrame, textvariable=minDepth)
minDepthEntry.insert(tkinter.END, 43)
minDepthEntry.grid()

maxDepth = tkinter.StringVar()
maxDepthLabel = tkinter.Label(minMaxDepthFrame, text="max")
maxDepthLabel.grid(sticky = 'W')
maxDepthEntry = tkinter.Entry(minMaxDepthFrame, textvariable=maxDepth)
maxDepthEntry.insert(tkinter.END, 79)
maxDepthEntry.grid()

#criando o frame da mesa
tableFrame = tkinter.Frame(constraintFrame, borderwidth=1, relief="solid")
tableFrame.grid(row = 0, column = 2, padx=10,pady=10)
tableLabel = tkinter.Label(tableFrame, text="Mesa(%)", )
tableLabel.grid(sticky = 'E')

minMaxTableFrame = tkinter.Frame(tableFrame)
minMaxTableFrame.grid()

minTable = tkinter.StringVar()
minTableLabel = tkinter.Label(minMaxTableFrame, text="min")
minTableLabel.grid(sticky = 'W')
minTableEntry = tkinter.Entry(minMaxTableFrame, textvariable=minTable)
minTableEntry.insert(tkinter.END, 43)
minTableEntry.grid()

maxTable = tkinter.StringVar()
maxTableLabel = tkinter.Label(minMaxTableFrame, text="max")
maxTableLabel.grid(sticky = 'W')
maxTableEntry = tkinter.Entry(minMaxTableFrame, textvariable=maxTable)
maxTableEntry.insert(tkinter.END, 95)
maxTableEntry.grid()

#criando o frame do preço
priceFrame = tkinter.Frame(constraintFrame, borderwidth=1, relief="solid")
priceFrame.grid(row = 0, column = 3, padx=10,pady=10)
priceLabel = tkinter.Label(priceFrame, text="Preço(U$)", )
priceLabel.grid(sticky = 'E')

minMaxPriceFrame = tkinter.Frame(priceFrame)
minMaxPriceFrame.grid()

minPrice = tkinter.StringVar()
minPriceLabel = tkinter.Label(minMaxPriceFrame, text="min")
minPriceLabel.grid(sticky = 'W')
minPriceEntry = tkinter.Entry(minMaxPriceFrame, textvariable=minPrice)
minPriceEntry.insert(tkinter.END, 326)
minPriceEntry.grid()

maxPrice = tkinter.StringVar()
maxPriceLabel = tkinter.Label(minMaxPriceFrame, text="max")
maxPriceLabel.grid(sticky = 'W')
maxPriceEntry = tkinter.Entry(minMaxPriceFrame, textvariable=maxPrice)
maxPriceEntry.insert(tkinter.END, 18823)
maxPriceEntry.grid()

#criando o frame da lapidação
cutFrame = tkinter.Frame(constraintFrame, borderwidth=1, relief="solid")
cutFrame.grid(row = 1, column = 0, padx=10,pady=10)
cutLabel = tkinter.Label(cutFrame, text="Lapidação", )
cutLabel.grid(sticky = 'E')

minMaxCutFrame = tkinter.Frame(cutFrame)
minMaxCutFrame.grid()

minCutLabel = tkinter.Label(minMaxCutFrame, text="min")
minCutLabel.grid(sticky = 'W')
scrollbar = tkinter.Scrollbar(minMaxCutFrame, orient = tkinter.VERTICAL)
minCutList = tkinter.Listbox(minMaxCutFrame, yscrollcommand=scrollbar.set)
scrollbar.config(command=minCutList.yview)
scrollbar.grid(row=1, column=1, sticky='ns')
for item in ['Fair', 'Good', 'Very Good', 'Premium', 'Ideal']:
    minCutList.insert('end', item)
minCutList.grid(row=1, column=0)
minCutList.config(height=1)
minCutList.select_set(0)

maxCutLabel = tkinter.Label(minMaxCutFrame, text="max")
maxCutLabel.grid(sticky = 'W')
scrollbar = tkinter.Scrollbar(minMaxCutFrame, orient = tkinter.VERTICAL)
maxCutList = tkinter.Listbox(minMaxCutFrame, yscrollcommand=scrollbar.set)
scrollbar.config(command=maxCutList.yview)
scrollbar.grid(row=3, column=1, sticky='ns')
for item in ['Fair', 'Good', 'Very Good', 'Premium', 'Ideal']:
    maxCutList.insert('end', item)
maxCutList.grid(row=3, column=0)
maxCutList.config(height=1)
maxCutList.select_set(4)

#criando o frame da cor
colorFrame = tkinter.Frame(constraintFrame, borderwidth=1, relief="solid")
colorFrame.grid(row = 1, column = 1, padx=10,pady=10)
colorLabel = tkinter.Label(colorFrame, text="Cor", )
colorLabel.grid(sticky = 'E')

minMaxColorFrame = tkinter.Frame(colorFrame)
minMaxColorFrame.grid()

minColor = tkinter.StringVar()
minColorLabel = tkinter.Label(minMaxColorFrame, text="min")
minColorLabel.grid(sticky = 'W')
scrollbar = tkinter.Scrollbar(minMaxColorFrame, orient = tkinter.VERTICAL)
minColorList = tkinter.Listbox(minMaxColorFrame, yscrollcommand=scrollbar.set)
scrollbar.config(command=minColorList.yview)
scrollbar.grid(row=1, column=1, sticky='ns')
for item in ['J', 'I', 'H', 'G', 'F', 'E', 'D']:
    minColorList.insert('end', item)
minColorList.grid(row=1, column=0)
minColorList.config(height=1)
minColorList.select_set(0)

maxColor = tkinter.StringVar()
maxColorLabel = tkinter.Label(minMaxColorFrame, text="max")
maxColorLabel.grid(sticky = 'W')
scrollbar = tkinter.Scrollbar(minMaxColorFrame, orient = tkinter.VERTICAL)
maxColorList = tkinter.Listbox(minMaxColorFrame, yscrollcommand=scrollbar.set)
scrollbar.config(command=maxColorList.yview)
scrollbar.grid(row=3, column=1, sticky='ns')
for item in ['J', 'I', 'H', 'G', 'F', 'E', 'D']:
    maxColorList.insert('end', item)
maxColorList.grid(row=3, column=0)
maxColorList.config(height=1)
maxColorList.select_set(6)

#criando o frame da pureza
clarityFrame = tkinter.Frame(constraintFrame, borderwidth=1, relief="solid")
clarityFrame.grid(row = 1, column = 2, padx=10,pady=10)
clarityLabel = tkinter.Label(clarityFrame, text="Pureza", )
clarityLabel.grid(sticky = 'E')

minMaxClarityFrame = tkinter.Frame(clarityFrame)
minMaxClarityFrame.grid()

minClarity = tkinter.StringVar()
minClarityLabel = tkinter.Label(minMaxClarityFrame, text="min")
minClarityLabel.grid(sticky = 'W')
scrollbar = tkinter.Scrollbar(minMaxClarityFrame, orient = tkinter.VERTICAL)
minClarityList = tkinter.Listbox(minMaxClarityFrame, yscrollcommand=scrollbar.set)
scrollbar.config(command=minClarityList.yview)
scrollbar.grid(row=1, column=1, sticky='ns')
for item in ['I3', 'I2', 'I1', 'SI2', 'SI1', 'VS2', 'VS1', 'VVS2', 'VVS1', 'IF', 'FL']:
    minClarityList.insert('end', item)
minClarityList.grid(row=1, column=0)
minClarityList.config(height=1)
minClarityList.select_set(0)

maxClarity = tkinter.StringVar()
maxClarityLabel = tkinter.Label(minMaxClarityFrame, text="max")
maxClarityLabel.grid(sticky = 'W')
scrollbar = tkinter.Scrollbar(minMaxClarityFrame, orient = tkinter.VERTICAL)
maxClarityList = tkinter.Listbox(minMaxClarityFrame, yscrollcommand=scrollbar.set)
scrollbar.config(command=maxClarityList.yview)
scrollbar.grid(row=3, column=1, sticky='ns')
for item in ['I3', 'I2', 'I1', 'SI2', 'SI1', 'VS2', 'VS1', 'VVS2', 'VVS1', 'IF', 'FL']:
    maxClarityList.insert('end', item)
maxClarityList.grid(row=3, column=0)
maxClarityList.config(height=1)
maxClarityList.select_set(10)

# tratando o csv
diamondsCsv = pandas.read_csv("diamonds.csv").head(1000)
del diamondsCsv["x"]
del diamondsCsv["y"]
del diamondsCsv["z"]

diamondsCsv = diamondsCsv.apply(updateColumnValues, axis=1)

def getCsv():
    return diamondsCsv

image = Image.open('lupa.png')
image = image.resize((80, 60), Image.ANTIALIAS)

img = ImageTk.PhotoImage(image)

button = tkinter.Button(constraintFrame, image = img, command = buttonCallback, bd=3, height=60, width = 80)

button.grid(row = 1, column = 3)


#mostrando a janela e iniciando captura de eventos
main.mainloop()


# In[ ]:




